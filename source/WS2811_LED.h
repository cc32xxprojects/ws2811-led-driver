/*
 * Contains WS2811 Function declarations and LED color data
 *
 *  Created on: Oct 1, 2017
 *      Author: Alexander
 */

#ifndef WS2811_LED_H_
#define WS2811_LED_H_

#define HIGH_BYTE(x) ((x >> 8) & 0xFF)
#define LOW_BYTE(x) (x & 0xFF)

#define MASK_BYTE_3(x) (uint32_t)(((uint32_t) x << 24) & (0xFF000000))
#define MASK_BYTE_2(x) (uint32_t)(((uint32_t) x << 16) & (0x00FF0000))
#define MASK_BYTE_1(x) (uint32_t)(((uint32_t) x << 8) & (0x0000FF00))
#define MASK_BYTE_0(x) (uint32_t)(((uint32_t) x) & (0x000000FF))


/* User defines */
#define WS2811_BIT_RATE (6000000) // 6 MHZ for .166 uS bit rate
#define WS2811_DATA_SIZE (32) // 8 Bit data size
#define WS2811_LOGIC_1 (0xF8)
#define WS2811_LOGIC_0 (0xC0)
#define WS2811_RESET (0x00000000)
#define WS2811_PIXEL_COLOR_LEN (8)
#define WS2811_BYTES_PER_PIXEL (24)
#define WS2811_RESET_OFFSET (0) /* Send 50 bytes low to stabilize SPI bus */
#define MAX_DMA_TRANSFER (1024) /* Maximum size of single DMA transfer */

/* Structure containing WS2811 individual pixel data */
typedef struct WS2811_Pixel {
    uint8_t redLed;
    uint8_t greenLed;
    uint8_t blueLed;
} WS2811_Pixel;

#define WS2811_PIXEL_SIZE (sizeof(WS2811_Pixel))

typedef struct WS2811_LEDStrip {
    /* Number of WS2811 pixels within strip */
    uint16_t len;
    /* Array of WS2811 pixels */
    WS2811_Pixel *pixel;

    /* Length of serialized WS2811 data*/
    uint16_t arrLen;
    /* Pointer to serialized LED Strip */
    uint32_t *ledStripArr;
} WS2811_LEDStrip;


/* WS2811 Specific functions */
WS2811_LEDStrip *WS2811_Pixel_open(uint16_t stripLen);
void WS2811_serialize(WS2811_LEDStrip *ledStrip);
bool WS2811_updateDisplay(WS2811_LEDStrip *ledStrip);
void WS2811_writeColor(WS2811_LEDStrip *ledStrip, uint32_t color);
void WS2811_convertByte(uint8_t ledByte, uint32_t *ledArr, uint16_t offset);

/* LED Gamma correction table
 * See: https://learn.adafruit.com/led-tricks-gamma-correction/the-quick-fix for reference
 */
static const uint8_t gamma8[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

/* Macros to extract specific byte values for each LED color */
#define WS2811_GREEN(COLOR) (gamma8[((COLOR >> 16) & 0xFF)])
#define WS2811_RED(COLOR) (gamma8[((COLOR >> 8) & 0xFF)])
#define WS2811_BLUE(COLOR) (gamma8[(COLOR & 0xFF)])

/* Color definitions based on the Fast LED library [0xRRGGBB]
https://github.com/FastLED/FastLED/wiki/Pixel-reference#colors
*/
#define WS2811_OFF 0x000000
#define WS2811_White   0xFFFFFF
#define WS2811_Red     0xFF0000
#define WS2811_Lime    0x00FF00
#define WS2811_Blue    0x0000FF
#define WS2811_Yellow  0xFFFF00
#define WS2811_Cyan    0x00FFFF
#define WS2811_Magenta 0xFF00FF
#define WS2811_Silver  0xC0C0C0
#define WS2811_Gray    0x808080
#define WS2811_Maroon  0x800000
#define WS2811_Olive   0x808000
#define WS2811_Green   0x008000
#define WS2811_Purple  0x800080
#define WS2811_Teal    0x008080
#define WS2811_Navy    0x000080



#endif /* WS2811_LED_H_ */
