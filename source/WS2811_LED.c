/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* XDC module Headers */
#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>

/* BIOS module Headers */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include <xdc/cfg/global.h>
#include <math.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
//#include <ti/drivers/SPI.h>
#include "SPICC32XXDMA_LED.h"

#include "Board.h"
#include "WS2811_LED.h"

#define WORDS_PER_CONTROLLER (6)
#define BYTES_PER_CONTROLLER (WORDS_PER_CONTROLLER * WS2811_DATA_SIZE)

/* Used to align heapmem/heapbuf buffers */
typedef union {
    double d;
    long l;
    void *p;
} AlignData;


SPI_Handle      spiHandle;
SPI_Params      spiParams;
SPI_Transaction spiTransaction;
bool            transferOK;

void convertUint8To32(uint8_t *u8arr, uint16_t u8len,  uint32_t *u32arr, uint16_t u32len);

static Void printHeapStats(IHeap_Handle heap);

static Void printHeapStats(IHeap_Handle heap)
{
    Memory_Stats stats;

    Memory_getStats(heap, &stats);
    System_printf("largestFreeSize = %d\n", stats.largestFreeSize);
    System_printf("totalFreeSize = %d\n", stats.totalFreeSize);
    System_printf("totalSize = %d\n", stats.totalSize);
    System_flush();
}



void WS2811_writeColor(WS2811_LEDStrip *ledStrip, uint32_t color)
{
    int pixelN;
    uint8_t redByte, greenByte, blueByte;

    /* Extract color byte values */
    redByte = WS2811_RED(color);
    greenByte = WS2811_GREEN(color);
    blueByte = WS2811_BLUE(color);

    /* Convert byte values to bit array */
    for (pixelN = 0; pixelN < ledStrip->len ; pixelN++) {
        ledStrip->pixel[pixelN].redLed = redByte;
        ledStrip->pixel[pixelN].greenLed = greenByte;
        ledStrip->pixel[pixelN].blueLed = blueByte;
    }
}

bool WS2811_Pixel_close(WS2811_LEDStrip *ledStrip)
{
    /* Check if driver is open */
    if (!ledStrip) {
        return (false);
    }

    IHeap_Handle heap = HeapMem_Handle_upCast(heap0);
    if (heap == NULL) {
        System_abort("Error allocating SPI heap!");
    }

    /* Close SPI Driver */
    SPI_close(spiHandle);

    System_printf("CLOSED  SPI status\n");
    System_flush();

    /* Print initial task0heap status */
    printHeapStats(heap);

    /* De-allocate LED Strip */
    Memory_free(heap, ledStrip->ledStripArr, sizeof(uint32_t) * ledStrip->arrLen);
    Memory_free(heap, ledStrip->pixel, (ledStrip->len * sizeof(WS2811_Pixel)));
    Memory_free(heap, ledStrip, sizeof(WS2811_LEDStrip));


    System_printf("CLOSED FINAL heap status\n");
    System_flush();

    /* Print initial task0heap status */
    printHeapStats(heap);
    return (true);
}


/* Input: Length of desired LED strip
*  Output: Pointer to LED Strip structure (initialized to 0)
*/
WS2811_LEDStrip *WS2811_Pixel_open(uint16_t numOfControllers)
{
    WS2811_LEDStrip *ledStrip;

    /* Pad data with reset on both edges */
    uint16_t ledStripArrLen = (numOfControllers * WORDS_PER_CONTROLLER);
    uint16_t totalPixelSize = (sizeof(WS2811_Pixel) * numOfControllers); 

    /* Attempt to allocate sufficient memory */
    IHeap_Handle heap = HeapMem_Handle_upCast(heap0);
    if (heap == NULL) {
        System_abort("Error allocating task0 heap!");
    }

    System_printf("Initial task0 heap status\n");
    System_flush();

    /* Print initial task0heap status */
    printHeapStats(heap);

    /* Allocate blocks from system heap */
    ledStrip = (WS2811_LEDStrip *) Memory_calloc(heap, sizeof(WS2811_LEDStrip), 0, NULL);

    ledStrip->pixel = (WS2811_Pixel *) Memory_calloc(heap, totalPixelSize, 0, NULL);

    ledStrip->ledStripArr = (uint32_t *) Memory_calloc(heap, sizeof(uint32_t) * ledStripArrLen, 0, NULL);


    System_printf("Final task0 heap status\n");
    System_flush();

    if (ledStrip && ledStrip->pixel && ledStrip->ledStripArr) {
        ledStrip->len = numOfControllers;
        ledStrip->arrLen = ledStripArrLen;
    }
    else {
        /* Free any memory from successful callocs */
        return (NULL);
    }

    /* Print task0Heap status */
    printHeapStats(heap);


    SPI_init();

    /* Initialize SPI parameters */
    SPI_Params_init(&spiParams);
    spiParams.dataSize = WS2811_DATA_SIZE;
    spiParams.transferMode = SPI_MODE_BLOCKING;
    spiParams.bitRate = WS2811_BIT_RATE;
    spiParams.mode = SPI_MASTER;

    spiHandle = SPI_open(Board_SPI0, &spiParams);
    if (spiHandle == NULL) {
        while (1); /* SPI_open() failed */
    }

    return (ledStrip);
}

/* 
WS2811 Data format: 24=bit
[R G B]
[1 byte Red] [1 byte Green0] [1 byte Blue]

5m 60 LEDs per meter = 300 LED's on the entire strip
Array size byte be ~(300 * (3 Bytes/LED)) = 900 bytes maximum
*/

/*
 * Update the WS2811 LED strip using the SPI module
 * Sends a reset signal (Low >= 50 us)
 * and follows with the data to be sent
 * Takes the serialized LED Data and sends it over SPI
 */
bool WS2811_updateDisplay(WS2811_LEDStrip *ledStrip)
{
    bool ret = true;

    /* Serialize the LED Strip */
    WS2811_serialize(ledStrip);


    int i;
/*
   for (i = 600; i < 1200;i++)
    {
        ledStrip->ledStripArr[i] = 0x00;
    }
*/


    /* Fill in transmission buffer */
    spiTransaction.count = 1200;
    spiTransaction.txBuf = ledStrip->ledStripArr;
    spiTransaction.rxBuf = NULL;

    transferOK = SPI_transfer(spiHandle, &spiTransaction);

    if (!transferOK) {
        /* Error in SPI or transfer already in progress */
        ret = false;
    }
    transferOK = SPI_transfer(spiHandle, &spiTransaction);

    if (!transferOK) {
        /* Error in SPI or transfer already in progress */
        ret = false;
    }


    return (ret);
}

#define BIT_TO_BYTE(X, SHIFT) ((X & (1 << SHIFT)) ? WS2811_LOGIC_1 : WS2811_LOGIC_0)

/* Convert a byte color value to it's WS2811 equivalent 8 byte data stream */
// Red green and blue byte data
void WS2811_convertByte(uint8_t ledByte, uint32_t *ledArr, uint16_t offset)
{
    uint32_t tempU32 = 0;
    //1101 0111b
    tempU32 |= MASK_BYTE_0(BIT_TO_BYTE(ledByte, 4));
    tempU32 |= MASK_BYTE_1(BIT_TO_BYTE(ledByte, 5));
    tempU32 |= MASK_BYTE_2(BIT_TO_BYTE(ledByte, 6));
    tempU32 |= MASK_BYTE_3(BIT_TO_BYTE(ledByte, 7));
    ledArr[offset] = tempU32;

    tempU32 = 0;
    tempU32 |= MASK_BYTE_0(BIT_TO_BYTE(ledByte, 0));
    tempU32 |= MASK_BYTE_1(BIT_TO_BYTE(ledByte, 1));
    tempU32 |= MASK_BYTE_2(BIT_TO_BYTE(ledByte, 2));
    tempU32 |= MASK_BYTE_3(BIT_TO_BYTE(ledByte, 3));
    ledArr[offset + 1] = tempU32;

}

/* Converts a LEDStrip structure into an array */
void WS2811_serialize(WS2811_LEDStrip *ledStrip)
{
    int pixelArrItr = 0;
    int pixelN;

    for (pixelN = 0; pixelN < ledStrip->len; pixelN++) {
        WS2811_convertByte(ledStrip->pixel[pixelN].redLed, ledStrip->ledStripArr, pixelArrItr);
        pixelArrItr+=2;

        WS2811_convertByte(ledStrip->pixel[pixelN].greenLed, ledStrip->ledStripArr, pixelArrItr);
        pixelArrItr+=2;

        WS2811_convertByte(ledStrip->pixel[pixelN].blueLed, ledStrip->ledStripArr, pixelArrItr);
        pixelArrItr+=2;
    }
}
