/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* XDC module Headers */
#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>

/* BIOS module Headers */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include <xdc/cfg/global.h>
#include <math.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>

#include "Board.h"
#include "WS2811_LED.h"

#define TASKSTACKSIZE   (2048)      /* Stack size for constructed tasks */
#define X_LED_STRIP_LEN (800)

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];


/*
 *  ======== task0Fxn ========
 */
Void task0Fxn(UArg arg0, UArg arg1)
{
    /* Create an LED Strip object */
    WS2811_LEDStrip *xLedStrip;

    xLedStrip = WS2811_Pixel_open(X_LED_STRIP_LEN);

    if (xLedStrip == NULL) {
        System_abort("Error allocating LED Strip");
    }

    /* Write a specific colors data to the LED strip */

    while (1) {
        //WS2811_DarkGreen
/*        WS2811_writeColor(xLedStrip, 0x000000);
        WS2811_updateDisplay(xLedStrip);
        GPIO_toggle(Board_GPIO_LED0);
        sleep(1);*/
        WS2811_writeColor(xLedStrip, 0x00f000);
        WS2811_updateDisplay(xLedStrip);
        GPIO_toggle(Board_GPIO_LED0);
        sleep(1);

        WS2811_writeColor(xLedStrip, 0xf00000);
        WS2811_updateDisplay(xLedStrip);
        GPIO_toggle(Board_GPIO_LED0);
        sleep(1);


        WS2811_writeColor(xLedStrip, 0x0000f0);
        WS2811_updateDisplay(xLedStrip);
        GPIO_toggle(Board_GPIO_LED0);
        sleep(1);


    }

}




/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    /* Construct BIOS objects */
    Task_Params taskParams;
    /* Construct writer/reader Task threads */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)task0Fxn, &taskParams, NULL);

    /* Call driver init functions */
    GPIO_init();


    while (1) {
        sleep(1000);
    }
}
